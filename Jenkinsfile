// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
// image can be used for promoting...
def ALPINE_IMAGE
def UBUNTU_IMAGE
def DEBUG = false
def RELEASE_VERSION
def RELEASE_BUILD

def testImage(imageName) {
    withDockerContainer(imageName) {
        sh "docker version"
        sh "gitversion help"
        sh "cmdclient help"
        sh "kubectl help"
        sh "helm help"
        sh "s2i version"
        sh "sonar-scanner -v"
        sh "acp help"
        sh "yq --version"
        sh "jq --version"
    }
}
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }

    //(optional) 环境变量
    environment {
        FOLDER = '$WORKSPACE'

        // for building an scanning
        REPOSITORY = "builder_tools"
        OWNER = "mathildetech"
        // sonar feedback user
        // needs to change together with the credentialsID
        BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
        SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
        DINGDING_BOT = "jenkins-group-bot"
        TAG_CREDENTIALS = "alaudabot-bitbucket"
        GOPATH = "${WORKSPACE}"
        PATH = "${PATH}:${WORKSPACE}/bin"

        IMAGE_REPOSITORY = "index.alauda.cn/alaudaorg/builder-tools"
        IMAGE_REPOSITORY_CREDENTIALS = "alaudacn-daniel"
        SCANNER_VERSION = "3.2.0.1227"
        HELM_VERSION = "v2.10.0"
        YQ_VERSION = "2.1.1"
        JQ_VERSION = "1.5"
    }
    // stages
    stages {
        stage('Checkout') {
            steps {
                script {
                    // checkout code
                    def scmVars = checkout scm
                    // extract git information
                    env.GIT_COMMIT = scmVars.GIT_COMMIT
                    env.GIT_BRANCH = scmVars.GIT_BRANCH
                    GIT_COMMIT = "${scmVars.GIT_COMMIT}"
                    GIT_BRANCH = "${scmVars.GIT_BRANCH}"
                    RELEASE_VERSION = readFile('.version').trim()
                    RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"
                    if (GIT_BRANCH != "master") {
                        def branch = GIT_BRANCH.replace("/","-").replace("_","-")
                        RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}".toLowerCase()
                    }
                }
                // moving project code for the specified folder
                sh """
                    go get -u github.com/alauda/gitversion
                    gitversion help
                """
                script {
                    if (GIT_BRANCH == "master") {
                        sh "gitversion patch ${RELEASE_VERSION} > patch"
                        RELEASE_BUILD = readFile("patch").trim()
                    }
                    echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
                    sh "rm bin/*"
                    sh "mkdir lib"
                    // sh "mkdir jre"
                }
            }
        }
        stage('Build Dependencies'){
            failFast true
            parallel {
                stage('Bergamot Scanner') {
                    steps {
                        checkout scm: [
                            $class: 'GitSCM', 
                            branches: [[name: '*/master']],
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'src/github.com/alauda/bergamot']],
                            userRemoteConfigs: [[url: 'https://github.com/alauda/bergamot']]
                        ]
                        sh "GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o bin/cmdclient github.com/alauda/bergamot/sonarqube/cmdclient"
                    }
                }
                stage('Gitversion') {
                    steps {
                        checkout scm: [
                            $class: 'GitSCM', 
                            branches: [[name: '*/master']],
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'src/github.com/alauda/gitversion']],
                            userRemoteConfigs: [[url: 'https://github.com/alauda/gitversion']]
                        ]
                        sh "GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -installsuffix cgo -o bin/gitversion github.com/alauda/gitversion"
                    }
                }
                stage('kubectl') {
                    steps {
                        sh '''
                            curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
                            mv kubectl bin
                        '''
                    }
                }
                stage('sonar scanner') {
                    steps {
                        sh """
                            wget https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-${SCANNER_VERSION}-linux.zip
                            unzip sonar-scanner-cli-${SCANNER_VERSION}-linux.zip
                            rm sonar-scanner-cli-${SCANNER_VERSION}-linux.zip
                            mv sonar-scanner-${SCANNER_VERSION}-linux/jre .
                            mv sonar-scanner-${SCANNER_VERSION}-linux/lib/*.jar lib
                            mv sonar-scanner-${SCANNER_VERSION}-linux/bin/* bin
                        """
                    }
                }
                stage('helm') {
                    steps {
                        sh """
                            curl https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O
                            tar -xvf helm-${HELM_VERSION}-linux-amd64.tar.gz
                            rm helm-${HELM_VERSION}-linux-amd64.tar.gz
                            mv linux-amd64/helm bin
                        """
                    }
                }
                stage('s2i') {
                    steps {
                        sh """
                            curl -L -O https://github.com/openshift/source-to-image/releases/download/v1.1.10/source-to-image-v1.1.10-27f0729d-linux-386.tar.gz
                            tar -xvf source-to-image-v1.1.10-27f0729d-linux-386.tar.gz
                            rm source-to-image-v1.1.10-27f0729d-linux-386.tar.gz
                            mv s2i sti bin
                        """
                    }
                }
                stage('acp') {
                    steps {
                        copyArtifacts filter: 'bin/linux*.zip', fingerprintArtifacts: true, projectName: '../../DevOps/acp/master', selector: lastSuccessful()
                        sh '''
                            mkdir ubuntubin alpinebin
                            cd bin
                            mv $(ls linux-alpine*.zip) ../alpinebin
                            mv $(ls linux-*.zip) ../ubuntubin
                            cd ../alpinebin && unzip $(ls *.zip) && rm *.zip
                            cd ../ubuntubin && unzip $(ls *.zip) && rm *.zip
                        '''
                    }
                }
                stage('yq') {
                    steps {
                        // https://github.com/mikefarah/yq/releases
                        sh "curl -O -L https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64"
                        sh "mv yq_linux_amd64 bin/yq"
                    }
                }
                stage('jq') {
                    steps {
                        // https://github.com/stedolan/jq/releases
                        sh "curl -O -L https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64"
                        sh "mv jq-linux64 bin/jq"
                    }
                }
            }
        }
        stage('Images'){
            failFast true
            parallel {
                stage('alpine3.8') {
                    steps {
                        script {
                            sh "chmod +x bin/*"
                            sh "chmod +x alpinebin/*"
                            ALPINE_IMAGE = deploy.dockerBuild(
                                "alpine3.8/Dockerfile",
                                ".",
                                IMAGE_REPOSITORY,
                                "alpine-${RELEASE_BUILD}",
                                IMAGE_REPOSITORY_CREDENTIALS
                            ).setArg("commit_id", "${GIT_COMMIT}").setArg("version", "${RELEASE_BUILD}")
                            ALPINE_IMAGE.start().push()
                        }
                    }
                }
                stage('ubuntu16..04') {
                    steps {
                        script {
                            sh "chmod +x bin/*"
                            sh "chmod +x ubuntubin/*"
                            UBUNTU_IMAGE = deploy.dockerBuild(
                                "ubuntu16.04/Dockerfile",
                                ".",
                                IMAGE_REPOSITORY,
                                "ubuntu-${RELEASE_BUILD}",
                                IMAGE_REPOSITORY_CREDENTIALS
                            ).setArg("commit_id", "${GIT_COMMIT}").setArg("version", "${RELEASE_BUILD}")
                            UBUNTU_IMAGE.start().push()
                        }
                    }
                }
            }
        }
        stage('Tests') {
            parallel {
                stage('alpine3.8') {
                    steps {
                        testImage(ALPINE_IMAGE.getImage())
                    }
                }
                stage('ubuntu16.04') {
                    steps {
                        testImage(UBUNTU_IMAGE.getImage())
                    }
                }
            }
        }
        stage('Promoting') {
            // limit this stage to master only
            when {
                expression {
                    GIT_BRANCH == "master"
                }
            }
            steps {
                script {
                    ALPINE_IMAGE.push("alpine")
                    UBUNTU_IMAGE.push("ubuntu")

                    // adding tag to the current commit
                    withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                        sh "git tag -l | xargs git tag -d" // clean local tags
                        sh """
                            git config --global user.email "alaudabot@alauda.io"
                            git config --global user.name "Alauda Bot"
                        """
                        def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                        sh "git fetch --tags ${repo}" // retrieve all tags
                        sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                        sh("git push ${repo} --tags")
                    }
                    // build job: '../../charts-pipeline', parameters: [
                    // [$class: 'StringParameterValue', name: 'CHART', value: 'devops'],
                    // [$class: 'StringParameterValue', name: 'VERSION', value: CHART_VERSION],
                    // [$class: 'StringParameterValue', name: 'COMPONENT', value: 'apiserver'],
                    // [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                    // [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                    // [$class: 'StringParameterValue', name: 'ENV', value: ''],
                    // ], wait: false
                }
            }
        }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
            echo "s"
            script {
                if (GIT_BRANCH == "master") {
                    deploy.notificationSuccess(REPOSITORY, DINGDING_BOT, "构建完成了", RELEASE_BUILD)
                } else {
                    deploy.notificationSuccess(REPOSITORY, DINGDING_BOT, "PR构建完成了", RELEASE_BUILD)
                }
            }
        }
        // 失败
        failure {
            echo "f"

            // check the npm log
            // fails lets check if it
            script {
                deploy.notificationFailed(REPOSITORY, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
            }
        }
    }
}
